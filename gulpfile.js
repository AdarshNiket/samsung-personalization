'use strict';

var browserSync = require('browser-sync');
var del = require('del');
var gulp = require('gulp');
var gutil = require('gulp-util');
var historyApi    = require('connect-history-api-fallback');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var webserver = require('gulp-webserver');
var sourcemaps = require('gulp-sourcemaps');
var htmllint = require('gulp-htmllint');
var tslint = require('gulp-tslint');
var sasslint = require('gulp-sass-lint');
var ts = require('gulp-typescript');
var webpack = require('webpack');
var WebpackServer = require('webpack-dev-server');

var tscConfig = ts.createProject('tsconfig.json');
// var tscConfig = require('./tsconfig.json');

var appSrc = 'src';

// var paths = {
//   src: {
//     ts: 'src/**/*.ts'
//   },

//   target: 'target'
// };

// var config = {
//   browserSync: {
//     files: [paths.target + '/**/*'],
//     notify: false,
//     open: false,
//     port: 3000,
//     reloadDelay: 500,
//     server: {
//       baseDir: paths.target
//     }
//   },
// 	tslint: {
// 		report: {
// 			options: {emitError: true},
// 			type: 'verbose'
// 		}
// 	},
//    webpack: {
//     dev: './webpack.dev',
//     dist: './webpack.dist'
//   }
// };

/*Gulp Tasks*/
// Copies dependencies from node module to local lib folder(can copy minfied file as well).
gulp.task('copylibs', function() {
  return gulp
    .src([
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
      'node_modules/rxjs/bundles/Rx.min.js',
      'node_modules/angular/angular.js',
      'node_modules/angular-ui-router/release/angular-ui-router.js',
      'node_modules/bootstrap/dist/js/bootstrap.min.js',
			'node_modules/jquery/dist/jquery.min.js'
      	])
    .pipe(gulp.dest(appSrc + '/resources/js/lib'));
});

// task to merge all sass file and convert it into one css file.
gulp.task('sass', function() {
    return gulp.src(['src/resources/css/sass/**/*.scss'])
    	.pipe(sourcemaps.init())
    	.pipe(sass({ outputStyle: 'expanded' }))
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(concat('app.css'))
    	.pipe(sourcemaps.write({
        addComment:false
      }))
      .pipe(gulp.dest(appSrc + '/resources/css'));
});

// task to start server.
gulp.task('webserver', function(){ 
	gulp.src(appSrc)
	.pipe(webserver({
		livereload:true,
		open:false
	}));
});

// task to review ts code using tslint
gulp.task('ts-lint', function(){
	return gulp.src('appSrc/**/*.ts')
		.pipe(tslint({
      formatter:"verbose",
      configuration:"tslint.json"
    }))
		.pipe(tslint.report());
	});

// // task to review html code using htmllint
gulp.task('html-lint', function(){
  return gulp.src('appSrc/**/*.html')
        .pipe(htmllint({}, function(filepath, issues){
          if(issues.length>0){
            issues.forEach(function(issue){
              gutil.log(gutil.colors.cyan('[gulp-htmllint]') + gutil.colors.white(filepath + '[' + issue.line + ',' + issue.column + ']:') + gutil.colors.red('(' + issue.code + ') ' + issue.msg));
            });
            process.exitCode = 1;
          }
        }));
});

// task to convert typescript code into javascript code and merge it into one js file.
gulp.task('tstojs', function(){
  return tscConfig.src()
        .pipe(sourcemaps.init())
        .pipe(tscConfig())
        .pipe(concat('bundle.js'))
        .pipe(sourcemaps.write({
          addComment:false
        }))
        .pipe(gulp.dest(appSrc + '/resources/js'));
});

// task to review sass code using sass-lint
// gulp.task('sass-lint', function(){
//   return gulp.src('src/resources/css/sass/**/*.s+(a|c)ss')
//         .pipe(sassLint())
//         .pipe(sassLint.format())
//         .pipe(sassLint.failOnError())
// });

// task to continuously check for changes made. 
gulp.task('watch', function () {
   gulp.watch(appSrc + '/resources/css/sass/**/*.scss', ['sass']);
});

gulp.task('default', ['html-lint', 'sass', 'ts-lint', 'webserver', 'watch']);




// gulp.task('clean.target', function(){
//   del(paths.target);
// });


// gulp.task('lint', function() {
//   return gulp.src(paths.src.ts)
//     .pipe(tslint())
//     .pipe(tslint.report(
//       config.tslint.report.type,
//       config.tslint.report.options
//     ));
// });


// gulp.task('serve', function(done){
//   config.browserSync.server.middleware = [historyApi()];
//   browserSync.create()
//     .init(config.browserSync, done);
// });


// gulp.task('serve.dev', function(done){
//   let conf = require(config.webpack.dev);
//   let compiler = webpack(conf);
//   let server = new WebpackServer(compiler, conf.devServer);
//   let host = conf.devServer.host;
//   let port = conf.devServer.port;

//   server.listen(port, host, function(){
//     gutil.log(gutil.colors.gray('-------------------------------------------'));
//     gutil.log('WebpackDevServer:', gutil.colors.magenta(`http://${host}:${port}`));
//     gutil.log(gutil.colors.gray('-------------------------------------------'));
//     done();
//   });
// });


// gulp.task('ts', function(done){
//   let conf = require(config.webpack.dist);
//   webpack(conf).run(function(error, stats){
//     if (error) throw new gutil.PluginError('webpack', error);
//     gutil.log(stats.toString(conf.stats));
//     done();
//   });
// });


// //===========================
// //  BUILD
// //---------------------------
// gulp.task('build', gulp.series(
//   'clean.target',
//   'ts'
// ));


// //===========================
// //  DEVELOP
// //---------------------------
// gulp.task('default', gulp.task('serve.dev'));


// //===========================
// //  RELEASE
// //---------------------------
// gulp.task('dist', gulp.series(
//   'lint',
//   'build'
// ));