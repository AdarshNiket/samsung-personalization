angular.module('myApp', ['ui.router']);
angular.module('myApp').config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
	$stateProvider
	.state('home', {
		url:'/home',
		templateUrl:'components/home/home.html'
	})
	.state('gallery', {
		url:'/gallery',
		templateUrl:'components/gallery/gallery.html'
	})
	.state('landing', {
		url:'/landing',
		templateUrl:'components/landing/landing.html'
	})
	.state('suit', {
		url:'/suit',
		templateUrl:'components/popularApps/suit.html'
	})
	.state('finish', {
		url:'/finish',
		templateUrl:'components/finalize/finish.html'
	})
	.state('personalised', {
		url:'/personalised',
		templateUrl:'components/personalised/personalised.html'
	})
	$urlRouterProvider.otherwise('/');
}]);