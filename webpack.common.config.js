var webpack = require('webpack')
var autoprefixer = require('autoprefixer');
var path = require('path');

// plugins
var OccurenceOrderPlugin = webpack.optimize.OccurenceOrderPlugin;
var ProvidePlugin = webpack.ProvidePlugin;

module.exports = {
    cache: true,
    debug: true,
    devtool: 'source-map',

    entry: {
        main: [
            './src/main'
        ],
        vendor: [
            'core-js',
            'reflect-metadata',
            'zone.js',
            '@angular/common',
            '@angular/core',
            '@angular/http',
            '@angular/platform-browser',
            '@angular/router',
            'rxjs'
        ]
    },

    output: {
        filename: 'bundle.js',
        path: path.resolve('./src/resources/js'),
        publicPath: undefined
    },

    resolve: {
        extensions: ['', '.ts', '.js', '.min.js'],
        modulesDirectories: ['node_modules', 'src/components', 'src/vendor'],
        root: path.resolve('./src')
    },

    // module:{
    //   loaders:[
    //     {test:/\.ts$/, loader:['awesome-typescript-loader', 'angular2-template-loader']}
    //   ],
    //   preLoaders:[
    //     {test:/\.js$/, loader:"source-map-loader"}
    //   ]
    // },

    module: {
        loaders: [
            {test: /\.html$/, loader: 'html'},
            {test: /\.scss$/, loaders: ['style', 'css', 'postcss', 'resolve-url', 'sass']},
            {test: /\.(jpe?g|png|gif|svg|cur)(\?.*?)?$/i, loaders: [
                'file?hash=sha512&digest=hex&name=[hash].[ext]',
                'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
            ]},
            { test: /\.(ttf|eot|woff(2)?)(\?v=[0-9]\.[0-9](\.[0-9])?)?|(\?.*?)]$/, loader: 'file-loader' },
            { test: /\.json$/, loader: 'json' }
        ],

        noParse: [
            /@angular\/*\/bundles\/.+/
        ]
    },

    htmlLoader: {
        minimize: true,
        removeAttributeQuotes: false,
        caseSensitive: true,
        customAttrSurround: [ [/#/, /(?:)/], [/\*/, /(?:)/], [/\[?\(?/, /(?:)/] ],
        customAttrAssign: [ /\)?]?=/ ]
    },

    postcss: function() {
        return [autoprefixer({browsers: ['last 3 versions', 'Firefox ESR']})];
    },

    sassLoader: {
        outputStyle: undefined,
        precision: 10,
        sourceComments: false
    },

    plugins: [
        new OccurenceOrderPlugin(),
        new ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery',
            "Tether": 'tether',
            "window.Tether": "tether"
        })
    ],
};
